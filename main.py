from mollib import atom, trafile, cabs, utils, cluster
import os
import numpy as np
import argparse
import logging

parser = argparse.ArgumentParser(
    prog='ContactClust',
)

parser.add_argument(
    'TRAJECTORY',
    help='load trajectory file [.cbs]'
)

parser.add_argument(
    '-u', '--contact-cutoff',
    metavar='VAL',
    default=6.0,
    type=float,
    help='set contact cutoff in Angstroms; default: %(default)s'
)

parser.add_argument(
    '-k', '--number-of-clusters',
    metavar='NUM',
    type=int,
    default=10,
    help='number of clusters to be saved; default: %(default)s'
)

parser.add_argument(
    '-g', '--sequence-gap',
    default=0,
    metavar='NUM',
    type=int,
    help='set sequence gap for two residues to be in contact; default: %(default)s'
)

parser.add_argument(
    '--method',
    choices=['single', 'complete', 'average'],
    default='complete',
    help='choose clustering method; default: %(default)s'
)

parser.add_argument(
    '-t', '--threshold',
    type=float,
    metavar='VAL',
    default=0.5,
    help='choose clustering threshold; default: %(default)s'
)

parser.add_argument(
    '--metric',
    default='jaccard',
    metavar='METRIC',
    help='choose distance metric; see '
         'https://docs.scipy.org/doc/scipy/reference/generated/scipy.spatial.distance.pdist.html'
         ' for the list of available metrics; default: %(default)s'
)

parser.add_argument(
    '-m', '--min-contacts',
    default=1,
    type=int,
    help='minimal number of contacts for structure to be kept for clustering; default: %(default)s'
)

parser.add_argument(
    '-f', '--filtering',
    default=1000,
    type=int,
    help='number of structures to be kept for clustering (sorted by number of contacts); default: %(default)s'
)

parser.add_argument(
    '-c', '--criterion',
    default='distance',
    help='criterion to form flat clusters; default: %(default)s '
         'see https://docs.scipy.org/doc/scipy/reference/generated/scipy.cluster.hierarchy.fcluster.html '
         'for list of available criteria'
)

parser.add_argument(
    '-s', '--min-size',
    metavar='NUM',
    default=2,
    type=int,
    help='minimal number of structures for cluster to be formed; default: %(default)s'
)

parser.add_argument(
    '--ranking-property',
    metavar='PROP',
    choices=['size', 'density'],
    default='density',
    help='choose cluster ranking property; ; default: %(default)s'
)

parser.add_argument(
    '-w', '--workdir',
    default='.',
    metavar='PATH',
    help='set working directory, default: %(default)s'
)

parser.add_argument(
    '-o', '--output',
    metavar='SELECTION',
    default='A',
    help='save output pdb files: (R)eplicas, (C)lusters, (M)odels, (N)one, (A)ll; default: %(default)s. '
         'Example: -oRC will save replicas and clusters.'
)

parser.add_argument(
    '-q', '--quiet',
    action='store_true',
    help='Set verbosity to ERROR'
)

# parse commandline args
args = parser.parse_args()

# logger config
logging.basicConfig(
    level=logging.ERROR if args.quiet else logging.DEBUG
)

# load cbs file
traj = trafile.Trafile.load(args.TRAJECTORY)

# init CABS lattice
LATTICE = cabs.CabsLattice()

# rebuild trajectory to CABS representation
traj.coordinates = LATTICE.rebuild_trafile(trafile=traj)
traj.template = LATTICE.rebuild_template(traj.template)
traj.LATTICE_UNIT = 1.0

# create index mask
length = traj.coordinates.shape[2] // 3
mask = np.array([[np.abs(i - j) > args.sequence_gap for i in range(length)] for j in range(length)])

# calculate contact maps
cmaps = np.array([
    [utils.DistanceMatrix(frame).contact_map(args.contact_cutoff) & mask for frame in replica]
    for replica in traj.coordinates[:, :, 2::3, :]
])

clusters = cluster.Clustering(
    cmaps=cmaps,
    method=args.method,
    metric=args.metric,
    threshold=args.threshold,
    criterion=args.criterion,
    min_size=args.min_size,
    min_contacts=args.min_contacts,
    filtering=args.filtering
).get_clusters(
    max_clusters=args.number_of_clusters,
    prop=args.ranking_property
)

reference = traj[clusters[0].medoid['replica']][clusters[0].medoid['frame']]

# save replicas
if 'R' in args.output or 'A' in args.output:
    for index, replica in enumerate(traj.trajectories, 1):
        replica.fit_to(reference)
        filename = os.path.join(args.workdir, f'replica_{index}.pdb')
        logging.debug(f'Saving {filename}')
        with open(filename, 'w') as f:
            f.write(replica.pdb)

# save clusters
if 'C' in args.output or 'A' in args.output:
    for index, clust in enumerate(clusters, 1):
        tra_clust = atom.Trajectory(
            template=traj.template,
            coordinates=traj.coordinates[clust.items]
        )
        tra_clust.fit_to(reference)
        filename = os.path.join(args.workdir, f'cluster_{index}.pdb')
        logging.debug(f'Saving {filename}')
        with open(filename, 'w') as f:
            f.write(tra_clust.pdb)

# save models
if 'M' in args.output or 'A' in args.output:
    for index, clust in enumerate(clusters, 1):
        filename = os.path.join(args.workdir, f'model_{index}.pdb')
        logging.debug(f'Saving {filename}')
        with open(filename, 'w') as f:
            f.write(
                traj[clust.medoid['replica']][clust.medoid['frame']].pdb
            )
