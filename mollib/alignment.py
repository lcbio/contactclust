"""
Module for pairing and aligning chains within two structures of protein complexes.
"""
import logging
import io
import numpy as np
from cached_property import cached_property
from scipy.optimize import linear_sum_assignment as lsa
from mollib import swalign


blosum62 = '''
   A  R  N  D  C  Q  E  G  H  I  L  K  M  F  P  S  T  W  Y  V  B  Z  X  *
A  4 -1 -2 -2  0 -1 -1  0 -2 -1 -1 -1 -1 -2 -1  1  0 -3 -2  0 -2 -1  0 -4 
R -1  5  0 -2 -3  1  0 -2  0 -3 -2  2 -1 -3 -2 -1 -1 -3 -2 -3 -1  0 -1 -4 
N -2  0  6  1 -3  0  0  0  1 -3 -3  0 -2 -3 -2  1  0 -4 -2 -3  3  0 -1 -4 
D -2 -2  1  6 -3  0  2 -1 -1 -3 -4 -1 -3 -3 -1  0 -1 -4 -3 -3  4  1 -1 -4 
C  0 -3 -3 -3  9 -3 -4 -3 -3 -1 -1 -3 -1 -2 -3 -1 -1 -2 -2 -1 -3 -3 -2 -4 
Q -1  1  0  0 -3  5  2 -2  0 -3 -2  1  0 -3 -1  0 -1 -2 -1 -2  0  3 -1 -4 
E -1  0  0  2 -4  2  5 -2  0 -3 -3  1 -2 -3 -1  0 -1 -3 -2 -2  1  4 -1 -4 
G  0 -2  0 -1 -3 -2 -2  6 -2 -4 -4 -2 -3 -3 -2  0 -2 -2 -3 -3 -1 -2 -1 -4 
H -2  0  1 -1 -3  0  0 -2  8 -3 -3 -1 -2 -1 -2 -1 -2 -2  2 -3  0  0 -1 -4 
I -1 -3 -3 -3 -1 -3 -3 -4 -3  4  2 -3  1  0 -3 -2 -1 -3 -1  3 -3 -3 -1 -4 
L -1 -2 -3 -4 -1 -2 -3 -4 -3  2  4 -2  2  0 -3 -2 -1 -2 -1  1 -4 -3 -1 -4 
K -1  2  0 -1 -3  1  1 -2 -1 -3 -2  5 -1 -3 -1  0 -1 -3 -2 -2  0  1 -1 -4 
M -1 -1 -2 -3 -1  0 -2 -3 -2  1  2 -1  5  0 -2 -1 -1 -1 -1  1 -3 -1 -1 -4 
F -2 -3 -3 -3 -2 -3 -3 -3 -1  0  0 -3  0  6 -4 -2 -2  1  3 -1 -3 -3 -1 -4 
P -1 -2 -2 -1 -3 -1 -1 -2 -2 -3 -3 -1 -2 -4  7 -1 -1 -4 -3 -2 -2 -1 -2 -4 
S  1 -1  1  0 -1  0  0  0 -1 -2 -2  0 -1 -2 -1  4  1 -3 -2 -2  0  0  0 -4 
T  0 -1  0 -1 -1 -1 -1 -2 -2 -1 -1 -1 -1 -2 -1  1  5 -2 -2  0 -1 -1  0 -4 
W -3 -3 -4 -4 -2 -2 -3 -2 -2 -3 -2 -3 -1  1 -4 -3 -2 11  2 -3 -4 -3 -2 -4 
Y -2 -2 -2 -3 -2 -1 -2 -3  2 -1 -1 -2 -1  3 -3 -2 -2  2  7 -1 -3 -2 -1 -4 
V  0 -3 -3 -3 -1 -2 -2 -3 -3  3  1 -2  1 -1 -2 -2  0 -3 -1  4 -3 -2 -1 -4 
B -2 -1  3  4 -3  0  1 -1  0 -3 -4  0 -3 -3 -2  0 -1 -4 -3 -3  4  1 -1 -4 
Z -1  0  0  1 -3  3  4 -2  0 -3 -3  1 -1 -3 -1  0 -1 -3 -2 -2  1  4 -1 -4 
X  0 -1 -1 -1 -2 -1 -1 -1 -1 -1 -1 -1 -1 -1 -2  0  0 -2 -1 -1 -1 -1 -1 -4 
* -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4  1
'''


class Alignment:

    SW = swalign.LocalAlignment(swalign.ScoringMatrix(text=blosum62), globalalign=True)

    def __init__(self, reference, query):
        self.reference = reference
        self.query = query
        self.reference_indices = []
        self.query_indices = []
        reference_lengths = [len(chain) for chain in self.reference.chains_list]
        query_lengths = [len(chain) for chain in self.query.chains_list]

        for ref_index, query_index in zip(*lsa(self.score_matrix, maximize=True)):
            alignment = self.alignment_matrix[ref_index][query_index]

            stream = io.StringIO()
            alignment.dump(out=stream)
            stream.seek(0)
            msg = '\n' + stream.read()
            logging.debug(msg)

            ref_indices, query_indices = self.get_indices(alignment)
            offset = sum(reference_lengths[:ref_index])
            self.reference_indices.extend([_ + offset for _ in ref_indices])
            offset = sum(query_lengths[:query_index])
            self.query_indices.extend([_ + offset for _ in query_indices])

    @cached_property
    def alignment_matrix(self):
        return [
            [self.SW.align(chain1.sequence, chain2.sequence) for chain2 in self.query.chains_list]
            for chain1 in self.reference.chains_list
        ]

    @cached_property
    def score_matrix(self):
        # warto pomyśleć co będzie lepsze: item.identity vs item.score
        return np.array([[item.identity for item in row] for row in self.alignment_matrix])

    @staticmethod
    def get_indices(alignment):
        ref_index = query_index = 0
        reference_indices = []
        query_indices = []
        for num, action in alignment.cigar:
            if action == 'I':
                query_index += num
            elif action == 'D':
                ref_index += num
            else:
                reference_indices.extend(range(ref_index, ref_index + num))
                query_indices.extend(range(query_index, query_index + num))
                ref_index += num
                query_index += num
        return reference_indices, query_indices
