import numpy as np
import string
import re
import logging

logger = logging.getLogger()

LONG_AA_NAMES = [
    'GLY', 'ALA', 'SER', 'CYS', 'VAL',
    'THR', 'ILE', 'PRO', 'MET', 'ASP',
    'ASN', 'LEU', 'LYS', 'GLU', 'GLN',
    'ARG', 'HIS', 'PHE', 'TYR', 'TRP'
]

SHORT_AA_NAMES = 'GASCVTIPMDNLKEQRHFYW'


def aa_long_to_short(resname):
    try:
        return SHORT_AA_NAMES[LONG_AA_NAMES.index(resname)]
    except ValueError:
        return 'U'


def aa_short_to_long(ch):
    try:
        return LONG_AA_NAMES[SHORT_AA_NAMES.index(ch)]
    except ValueError:
        return 'UNK'


class DistanceMatrix:

    """Class for efficient computation of distance matrix"""

    def __init__(self, coord1, coord2=None):
        l1 = len(coord1)
        csq1 = np.sum(coord1 * coord1, axis=1).reshape(1, -1)

        if coord2 is not None:
            l2 = len(coord2)
            csq2 = np.sum(coord2 * coord2, axis=1).reshape(1, -1)
            mat_a = np.repeat(csq1, l2, axis=0).transpose()
            mat_b = np.repeat(csq2, l1, axis=0)
            mat_c = np.dot(coord1, coord2.transpose())
        else:
            mat_a = np.repeat(csq1, l1, axis=0)
            mat_b = mat_a.transpose()
            mat_c = np.dot(coord1, coord1.transpose())

        self.data = (mat_a + mat_b - 2 * mat_c).astype(np.half)

    @property
    def d2(self):
        return self.data

    @property
    def distance_map(self):
        return np.sqrt(np.where(self.data < 1e-6, 0.0, self.data))

    def contact_map(self, distance):
        d2 = distance * distance
        return np.where(self.data < d2, True, False)

    def get_contacts(self, distance):
        return np.argwhere(self.contact_map(distance))


def kabsch(target, query, weights=None, concentric=False):

    """
    Function for the calculation of the best fit rotation.

    target     - a N x 3 np.array with coordinates of the reference structure
    query      - a N x 3 np.array with coordinates of the fitted structure
    weights    - a N-length list with weights - floats from [0:1]
    concentric - True/False specifying if target and query are centered at origin

    IMPORTANT: If weights are not None centering at origin should account for them.
    proper procedure: A -= np.average(A, 0, WEIGHTS)

    returns rotation matrix as 3 x 3 np.array
    """

    if not concentric:
        t = target - np.average(target, axis=0, weights=weights)
        q = query - np.average(query, axis=0, weights=weights)
    else:
        t = target
        q = query

    c = np.dot(weights * t.T, q) if weights else np.dot(t.T, q)
    v, s, w = np.linalg.svd(c)
    d = np.identity(3)
    if np.linalg.det(c) < 0:
        d[2, 2] = -1

    return np.dot(np.dot(w.T, d), v.T)


def rmsd(target, query=None, weights=None):
    _diff = target if query is None else query - target
    _rmsd = np.sqrt(np.average(np.sum(_diff ** 2, axis=1), axis=0, weights=weights))
    return _rmsd if _rmsd > 0.0001 else 0.


def next_letter(taken_letters):
    return re.sub('[' + taken_letters + ']', '', string.ascii_uppercase)[0]
