import numpy as np
from cached_property import cached_property
from scipy.spatial import distance
from scipy.cluster import hierarchy
import logging


class Cluster:

    INDICES = None

    def __init__(self, items, medoid, avg_distance):
        self.items = (
            self.INDICES[0][items],
            self.INDICES[1][items],
        )
        self.medoid = {
            'replica': self.INDICES[0][medoid],
            'frame': self.INDICES[1][medoid]
        }
        self.avg_distance = avg_distance

    @property
    def size(self):
        return len(self.items[0])

    @property
    def density(self):
        return self.size / self.avg_distance if self.avg_distance > 0 else self.size

    def __repr__(self):
        return f'{self.size} {self.density:.4f} {self.avg_distance:.4f}'


class Clustering:

    def __init__(self, cmaps, **kwargs):
        self.cmaps = cmaps
        self.method = kwargs.get('method')
        self.threshold = kwargs.get('threshold')
        self.criterion = kwargs.get('criterion')
        self.metric = kwargs.get('metric')
        self.min_contacts = kwargs.get('min_contacts')
        self.min_size = kwargs.get('min_size')
        self.filtering = kwargs.get('filtering')

    @cached_property
    def clusters(self):

        # filter cmaps
        logging.info('Filtering contact maps')
        contacts = np.sum(self.cmaps, axis=(2, 3))
        if self.filtering and len(contacts.reshape(-1)) >= self.filtering:
            self.min_contacts = np.sort(contacts, axis=None)[-self.filtering]
        logging.debug(f'Minimal number of contacts = {self.min_contacts}')
        Cluster.INDICES = np.where(contacts >= self.min_contacts)
        num_fmaps = len(Cluster.INDICES[0])
        logging.debug(f'{num_fmaps} contact maps were kept')
        filtered_maps = self.cmaps[Cluster.INDICES].reshape(num_fmaps, -1)

        # get flat clusters
        logging.info('Clustering contact maps')
        logging.debug(
            f'Method: {self.method}, metric: {self.metric}, criterion: {self.criterion}, threshold: {self.threshold}'
        )
        flat_clusters = hierarchy.fclusterdata(
            filtered_maps,
            t=self.threshold,
            method=self.method,
            metric=self.metric,
            criterion=self.criterion
        )

        # split flat clusters to items
        items_list = [np.where(flat_clusters == key)[0] for key in {*flat_clusters}]
        logging.debug(f'{len(items_list)} clusters formed')

        if self.min_size:
            logging.debug(f'Removing clusters with less than {self.min_size} members')
            items_list = [items for items in items_list if len(items) >= self.min_size]
            logging.debug(f'{len(items_list)} clusters were kept')

        # find medoids
        logging.info('Finding clusters\' medoids')
        metric = getattr(distance, self.metric)

        # helper function to find medoids
        # returns tuple(index of the medoid, average distance between cluster members)
        def get_cluster(items):

            if len(items) == 1:
                return Cluster(
                    items=items,
                    medoid=items[0],
                    avg_distance=0.
                )

            cluster_maps = filtered_maps[items]
            centroid = np.average(cluster_maps, axis=0)
            distances = [metric(centroid.astype(bool), cmap, w=centroid) for cmap in cluster_maps]

            return Cluster(
                items=items,
                medoid=items[np.argmin(distances)],
                avg_distance=np.average(distances)
            )

        return [get_cluster(items) for items in items_list]

    def get_clusters(self, max_clusters, prop):
        logging.info(f'Top {max_clusters} clusters sorted by {prop} were saved')
        return sorted(self.clusters, key=lambda x: getattr(x, prop), reverse=True)[
            slice(None, max_clusters if len(self.clusters) > max_clusters else None)
        ]
